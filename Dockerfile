﻿FROM mcr.microsoft.com/dotnet/aspnet:latest AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:latest AS build
WORKDIR /src
COPY ["GitOpsDockerTagScanner/GitOpsDockerTagScanner.csproj", "GitOpsDockerTagScanner/"]
RUN dotnet restore "GitOpsDockerTagScanner/GitOpsDockerTagScanner.csproj"
COPY . .
WORKDIR "/src/GitOpsDockerTagScanner"
RUN dotnet build "GitOpsDockerTagScanner.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "GitOpsDockerTagScanner.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "GitOpsDockerTagScanner.dll"]
