using HealthChecks.UI.Client;
using LanguageExt;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging.Console;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.AddSimpleConsole(options =>
{
    options.IncludeScopes = false;
    options.TimestampFormat = "[yyy-MM-dd HH:mm:ss.ffff]";
    options.ColorBehavior = LoggerColorBehavior.Enabled;
    options.SingleLine = true;
});
builder.Logging.AddFilter("*", LogLevel.Information);

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGenNewtonsoftSupport();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v2", new OpenApiInfo {Title = "GitOpsDockerTagScanner", Version = "v2"});
    c.EnableAnnotations();
});

builder
    .Services
    .AddHealthChecks()
    .AddMongoDb(
        mongodbConnectionString: builder.Configuration.GetValue<string>("MONGODB_CONNECTION") ??
                                 throw new ValueIsNullException("mongo db connectionString is null"),
        mongoDatabaseName: builder.Configuration.GetValue<string>("MONGODB_DATABASE") ??
                           throw new ValueIsNullException("mongo db databaseName is null"));

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.SerializeAsV2 = true; });
    app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "GitOpsDockerTagScanner"); });
}

app.MapHealthChecks("/healthz", new HealthCheckOptions
{
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();